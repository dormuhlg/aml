from aml_mnist import KerasMnist

from matplotlib import pyplot as plt
import argparse
import os

def train_eval_passive(hidden_layers, epochs, batch_size, sample_size):
    km = KerasMnist(hidden_layers, epochs, batch_size)
    km.load_data()
    km.label_data(sample_size)
    km.build_model()
    history, score_train, score_test = km.train_eval_model()
    print score_train, score_test
    km.plot_learning_curves(history)
    return score_train, score_test

def train_eval_active(hidden_layers, epochs, batch_size, source_epochs, source_size, query_size, labeled_size):
    experiments = ["passive","random", "diversity", "max", "margin", "entropy", "qbc"]
    #experiments = ["passive", "qbc"]
    if not os.path.exists("./experiments/"):
        os.makedirs("./experiments/")
    file = open("./experiments/results_log.txt", 'w')
    results = []
    km = KerasMnist(hidden_layers, epochs, batch_size)
    # Baseline: passive learning on 5000 examples
    km.load_data()
    km.label_data(labeled_size*10)
    km.build_model()
    _, _, score_test = km.train_eval_model()
    result = [score_test[1]]*(len(range(source_size + query_size, labeled_size + 1, query_size)) + 1)
    results.append(result)
    file.write("experiment: passive, test accuracy: {}\n".format(result[-1]))
    # Run active learning experiments
    for exp in experiments[1:]:
        km.load_data()
        km.build_model()
        print("running experiment: {}".format(exp))
        result = []
        if exp == "qbc":
            # build committee
            km.committee = []
            for i in [0, 2, 1]:
                member = KerasMnist(hidden_layers, epochs, batch_size)
                member.build_model(i)
                km.committee.append(member.model)
        # Train over the source data
        km.label_data(source_size)
        history, score_train, score_test = km.train_eval_model(source_epochs)
        print("size: {}, train: {}, test: {}".format(source_size, score_train, score_test))
        result.append(score_test[1])
        # Apply query strategy
        for i in range(source_size + query_size, labeled_size + 1, query_size):
            km.sort_unlabeled_data(key=exp)
            km.label_data(query_size)
            _, score_train, score_test = km.train_eval_model()
            result.append(score_test[1])
            if i%50 == 0 :
                print("size: {}, train: {}, test: {}".format(i, score_train, score_test))
        file.write("experiment: {}, test accuracy: {} \n".format(exp, result[-1]))
        km.save_data_images(exp)
        km.eval_per_digit(exp, file)
        results.append(result)
    file.close()
    km.plot_curves(experiments, results, source_size, query_size, labeled_size)
    return score_train, score_test


def get_args():
    parser = argparse.ArgumentParser(description='Trains and tests a neural classification model.')
    parser.add_argument('-m', '--mode', type=str, choices=['passive', 'active'], default='active',
                        help="""Passive will train a model on a large, randomly selected, training set and plot the 
                        learning curves. Active will train the models by querying the data in a sequential manner""")
    parser.add_argument('-st', '--source_size', type=int, default=50)
    parser.add_argument('-qs', '--query_size', type=int, default=10)
    parser.add_argument('-ls', '--labeled_size', type=int, default=500)
    parser.add_argument('-bs', '--batch_size', type=int, default=32)
    parser.add_argument('-e', '--epochs', type=int, default=10)
    parser.add_argument('-se', '--source_epochs', type=int, default=10)
    parser.add_argument('-hl', '--hidden_layers', nargs='+', type=int, default=(32, 16),
                        help="""A list of integers that defines the network architecture. 
                                Every integer x in the list adds another layer of size x to the network.""")
    return parser.parse_args()


def check_args(args):
    if -1 in args.hidden_layers:
        if args.mode == 'single':
            raise argparse.ArgumentTypeError(
                "using -1 in the hidden layers argument is allowed only in 'series' mode.")

    hidden_layers = []
    i = 0
    curr_net = []
    while i < len(args.hidden_layers):
        if args.hidden_layers[i] == -1:
            hidden_layers.append(curr_net)
            curr_net = []
        else:
            curr_net.append(args.hidden_layers[i])
        i += 1
    hidden_layers.append(curr_net)

    print("parsed network configurations: \n", hidden_layers)
    if len([curr_net for curr_net in hidden_layers
            if len(curr_net) == 0]) > 0:
        raise argparse.ArgumentTypeError(
            "all network configurations should have at least one hidden layer.")

    return hidden_layers


def main():
    args = get_args()
    hidden_layers = check_args(args)
    if args.mode == 'active':
        score_train, score_test = train_eval_active(
            hidden_layers[0], args.epochs, args.batch_size,
            args.source_epochs, args.source_size, args.query_size, args.labeled_size)
        print('train loss: {}, train accuracy: {}'.format(score_train[0], score_train[1]))
        print('test loss: {}, test accuracy: {}\n'.format(score_test[0], score_test[1]))
    else:
        score_train, score_test = train_eval_passive(
            hidden_layers[0], args.epochs,
            args.batch_size, args.labeled_size)
        print('train loss: {}, train accuracy: {}'.format(score_train[0], score_train[1]))
        print('test loss: {}, test accuracy: {}\n'.format(score_test[0], score_test[1]))

if __name__ == "__main__":
    main()

