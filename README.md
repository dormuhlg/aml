## Introduction

**Author**: Dor Muhlgay 

Based on:
[Active Learning Literature Survey (Settles 2010)](http://burrsettles.com/pub/settles.activelearning.pdf).

## Setup

Clone the project and install requirements

```bash
$ git clone https://gitlab.com/dormuhlg/aml 	# Clone project
$ cd aml
$ virtualenv --system-site-packages aml_env 	# Create a virtualenv
$ source ./aml_env/bin/activate
(aml_env)$ easy_install -U pip			# Install requirements 			 
(aml_env)$ pip install -r ./requirements.txt		
```
## Training

Running experiments: 

```bash
(aml_env)$ python ./aml_main.py -m active 		# Active learning experiments
(aml_env)$ python ./aml_main.py -m passive -ls 5000	# Passive learning experiments
```
Configuration flags: 
- -m training mode:
    - 'active'/'passive' learning
- -st source size:
    - the size of the source labeled set 
- -qs query size:
    - the number of labels queried at each iteration
- -ls labled size:
    - Active learning: the maximun data size to label (i.e, the queries' budget)
    - Passive learning: the training data size
- -hl hidden layers:
    - The neural-net's hidden layers' dimensions
- -bs batch size:
    - The neural net's batch size
- -e epochs:
    - Number of epochs
- -se epochs:
    - Number of epochs for the source-data training 

The script logs information about the experiments' progression on stdout.
The experiments' results will be saved to the directory `./experiments/`, containing the following
files:

- results_log.txt
    - Active learning experiments' results 
- active_accuracy.png
    - Plot of the active learning experiments accuracy as a function of the sample size
- ./{active learning experiment}/
    - the labled set queried by the active learning strategy
    - /{digit}/errors.png : a bar graph that illustrates the prediction errors per digits 
- passive_acc.png
    - Test and train accuracy of the passive learning experiment 
- passive_loss.png
    - Test and train loss of the passive learning experiment


