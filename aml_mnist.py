from __future__ import print_function

import numpy as np
from scipy.special import entr
from numpy.random import seed

import keras
from keras.datasets import mnist
from keras.models import Sequential, Model
from keras.layers import Dense, Activation, Input
from keras.optimizers import Adam

import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import matplotlib.image as mpimg

import os
class KerasMnist(object):
    def __init__(self, hidden_layers, epochs, batch_size):
        assert len(hidden_layers) > 0
        self.hidden_layer_dims = hidden_layers
        self.num_classes = 10
        self.input_dim = 784
        self.epochs = epochs
        self.batch_size = batch_size
        self.model = None
        self.committee = []
        self.init_w = None
        self.x_train_full = None
        self.y_train_full = None
        self.x_train_unlabeled = None
        self.y_train_unlabeled = None
        self.x_train = None
        self.y_train = None
        self.x_test = None
        self.y_test = None

    def load_data(self):
        # the data, split between train and test sets
        (self.x_train_full, self.y_train_full), (self.x_test, self.y_test) = mnist.load_data()
        self.x_train_unlabeled = self.x_train_full[:60000]
        self.y_train_unlabeled = self.y_train_full[:60000]
        self.x_train_unlabeled = self.x_train_unlabeled.reshape(60000, 784)
        self.x_test = self.x_test.reshape(10000, 784)
        self.x_train_unlabeled = self.x_train_unlabeled.astype('float32')
        self.x_test = self.x_test.astype('float32')
        self.x_train_unlabeled /= 255
        self.x_test /= 255
        # convert class vectors to binary class matrices
        self.y_train_unlabeled = keras.utils.to_categorical(self.y_train_unlabeled, self.num_classes)
        self.y_test = keras.utils.to_categorical(self.y_test, self.num_classes)
        self.x_train = self.x_train_unlabeled[:0]
        self.y_train = self.y_train_unlabeled[:0]

    def label_data(self, n):
        self.x_train = np.concatenate([self.x_train, self.x_train_unlabeled[:n]])
        self.y_train = np.concatenate([self.y_train, self.y_train_unlabeled[:n]])
        self.x_train_unlabeled = self.x_train_unlabeled[n:]
        self.y_train_unlabeled = self.y_train_unlabeled[n:]

    def margin(self, p):
        p.sort(axis=1)
        vals = [(prob[-1]-prob[-2]) for prob in p]
        return vals

    def diversity(self):
        labled_center = np.mean(self.x_train, axis=0)
        vals = [-np.linalg.norm(unlabled - labled_center) for unlabled in self.x_train_unlabeled]
        return vals

    def qbc(self):
        p = np.zeros((len(self.x_train_unlabeled), 10))
        for model in self.committee:
            preds = np.argmax(model.predict(self.x_train_unlabeled), axis=1)
            p[np.arange(len(self.x_train_unlabeled)), preds] = p[np.arange(len(self.x_train_unlabeled)), preds] + 1
        p = p / float(len(self.committee))
        return -entr(p).sum(axis=1)

    def sort_unlabeled_data(self, key):
        # basic selection methods that are independent from the model
        if key == "random":
            return
        # model-based selection methods
        p = self.model.predict(self.x_train_unlabeled)
        vals = []
        if key == "max":
            vals = np.amax(p, axis=1)
        elif key == "entropy":
            vals = -entr(p).sum(axis=1)
        elif key == "margin":
            vals = self.margin(p)
        elif key == "diversity":
            vals = self.diversity()
        elif key == "qbc":
            vals = self.qbc()
        pidx = np.argsort(vals)
        self.x_train_unlabeled = self.x_train_unlabeled[pidx]
        self.y_train_unlabeled = self.y_train_unlabeled[pidx]

    def build_model(self, s=1):
        '''
        MLP network with ReLU activations. For the last
        layer use the softmax activation. Initialize self.model
        as a Sequential model and add layers to it according to
        the class variables input_dim, hidden_layer_dims and num_classes.
        '''
        seed(s)
        model = Sequential()
        model.add(Dense(self.hidden_layer_dims[0], input_dim=self.input_dim, activation='relu'))
        for i in range(1, len(self.hidden_layer_dims)):
            model.add(Dense(self.hidden_layer_dims[i], activation='relu'))
        model.add(Dense(self.num_classes, activation='softmax'))
        self.model = model
        self.model.compile(loss='categorical_crossentropy', optimizer=Adam(), metrics=['accuracy'])
        self.init_w = self.model.get_weights()
        self.model.save_weights('model.h5')
        self.model.summary()

    def train_eval_model(self, epochs=None):
        if epochs:
            ep = epochs
        else:
            ep = self.epochs
        history = self.model.fit(self.x_train, self.y_train, batch_size=self.batch_size, epochs=ep, verbose=0,
                                 validation_data=(self.x_test, self.y_test))
        score_train = self.model.evaluate(self.x_train, self.y_train, verbose=0)
        score_test = self.model.evaluate(self.x_test, self.y_test, verbose=0)
        for member in self.committee:
            member.fit(self.x_train, self.y_train, batch_size=self.batch_size, epochs=self.epochs, verbose=0,
                                 validation_data=(self.x_test, self.y_test))
        return history, score_train, score_test


    def eval_per_digit(self, exp, file):
        for digit in range(0, 10, 1):
            y_digit = np.argmax(self.y_test, axis=1)
            x = self.x_test[y_digit == digit]
            preds = np.argmax(self.model.predict(x), axis=1)
            preds_h = np.histogram(preds, bins=range(0, 11, 1))
            errs = np.delete(preds_h[0], digit)
            file.write("error per digit {}: {}\n".format(digit, np.sum(errs)))
            x_bars = np.delete(range(0, 10, 1), digit)
            plt.bar(range(0,9,1), errs)
            plt.xlabel('prediction')
            plt.ylabel('count')
            plt.title('{}: errors count for: {}'.format(exp, digit))
            directory = "./experiments/" + exp + "/" + str(digit) + "/"
            if not os.path.exists(directory):
                os.makedirs(directory)
            figpath = directory + "/errors.png"
            plt.xticks(range(0,9,1), x_bars)
            plt.savefig(figpath)
            plt.clf()

    def save_data_images(self, exp):
        cnt = {0: 0, 1: 0, 2: 0, 3:0, 4:0, 5:0, 6:0, 7:0, 8:0, 9:0}
        labeled_size = len(self.x_train)
        for x, y in zip(self.x_train[::-1], self.y_train[::-1]):
            d = np.argmax(y)
            cnt[d] = cnt[d] + 1
            directory = "./experiments/" + str(exp) + "/" + str(d) + "/"
            if not os.path.exists(directory):
                os.makedirs(directory)
            full_path = directory + str(d) + "_" + str(cnt[d]) + ".png"
            mpimg.imsave(full_path, x.reshape(28, 28))
        self.x_train.reshape(labeled_size, 784)

    @staticmethod
    def plot_curves(experiments, results, source_size, query_size, labeled_size):
        plots = []
        sample_sizes = range(source_size, labeled_size+1, query_size)
        colors = ['grey', 'red', 'blue', 'green', 'purple', 'black', 'brown']
        colors = colors[:len(experiments)]
        patches = []
        for exp, result, col in zip(experiments, results, colors):
            plots.append(plt.plot(sample_sizes, result, lw=1.3, label=exp, color=col))
            patches = patches + [mpatches.Patch(color=col, label=exp)]
        plt.legend(handles=patches)
        plt.xlabel('sample size')
        plt.ylabel('accuracy')
        plt.savefig('./experiments/active_accuracy.png')

    @staticmethod
    def plot_learning_curves(history):
        history_dict = history.history
        for metric in ['loss', 'acc']:
            plt.clf()
            patches = []
            plots = []
            metric_values = history_dict[metric]
            val_metric_values = history_dict['val_' + metric]
            epochs = range(1, len(metric_values) + 1)
            plots.append(plt.plot(epochs, metric_values, lw=1.3, label='train', color='red'))
            patches = patches + [mpatches.Patch(color='red', label='train')]
            plots.append(plt.plot(epochs, val_metric_values, lw=1.3, label='test', color='blue'))
            patches = patches + [mpatches.Patch(color='blue', label='test')]
            plt.legend(handles=patches)
            plt.xlabel('epochs')
            plt.ylabel(metric)
            directory = "./experiments/"
            if not os.path.exists(directory):
                os.makedirs(directory)
            plt.savefig(directory + 'passive_' + metric + '.png')
